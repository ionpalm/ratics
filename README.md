# Ratics

An HTTP Web server for the Zig language.

## Inspiration

Taking the learnings from Tokio/Hyper, Gin, Gorilla Mux, Express, Nginx, and many other http router libraries to make a formidable contender in the Zig language.

